package ra;

public class Player {
    String name;

    int life = 0;

    int caughtRabbit = 0;

    public void setLife(int life) {
        this.life = life;
    }

    public int getLife() {
        return life;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
