package ra;

/*
    Riswan Agam
    04 - 08 - 2022
 */

import java.util.Scanner;

public class Hello {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        gameTitle();
        String name = sc.nextLine();
        welcome(name);

        Player player = new Player();
        player.setName(name);

        boolean isRunning = true;
        do {
            mainMenu();
            int input = sc.nextInt();
            Game game;
            switch (input) {
                case 0:
                    isRunning = false;
                    break;
                case 1:
                    game = new Game(new Table(Difficulty.EASY, new Rabbit(), player));
                    gameStart(sc, game);
                    break;
                case 2:
                    game = new Game(new Table(Difficulty.NORMAL, new Rabbit(), player));
                    gameStart(sc, game);
                    break;
                case 3:
                    game = new Game(new Table(Difficulty.HARD, new Rabbit(), player));
                    gameStart(sc, game);
                    break;
                default:
                    System.out.println("Pilihan tidak ada.");
            }
        } while(isRunning);

        sc.close();
    }

    public static void gameStart(Scanner sc, Game game) {
        System.out.println("Game dimulai temukan Kelinci yang ada didalam kotak !!");
        String[][] table = game.getClassTable().getTable();
        game.printTable(table, table.length, table[0].length);
        System.out.println();

        int maxRabbit = game.getClassTable().getTotalRabbit();

        while(game.totalRabbit < maxRabbit && game.playerLife >= 0) {
            System.out.print("Tebak ada dikotak berapa: ");
            int userInput = sc.nextInt();
            game.checkInput(userInput);
        }

        if(game.playerLife > 0 && game.totalRabbit == maxRabbit) {
            System.out.println("Selamat Kamu Menang !!!");
        } else {
            System.out.println("NOOOOB!!!");
        }
    }

    public static void gameTitle() {
        System.out.println("                  Rabbit Game");
        System.out.println("================================================");
        System.out.print("Masukan nama kamu: ");
    }

    public static void welcome(String msg) {
        System.out.println("================================================");
        System.out.println("Halo " + msg + ", selamat datang di Rabbit Game");
        System.out.println("================================================");
    }

    public static void mainMenu() {
        System.out.println("================================================");
        System.out.println("Pilih Kesulitan Game");
        System.out.println("1. Easy");
        System.out.println("2. Normal");
        System.out.println("3. Hard");
        System.out.println("0. Keluar Game");
        System.out.print("Masukan pilihan: ");
    }
}