package ra;

import java.util.*;

public class Table {

    private String[][] table;

    private Rabbit rabbit;

    private Player player;

    private int TOTAL_RABBIT = 0;

    // mengambil posisi kolom yang berupa angka
    private final HashMap<Integer, int[]> mapNumberPosition;
    private final ArrayList<Integer> allPosition;
    private final int DETAULT_ROW = 5;
    private final int DEFAULT_COLUMN = 9;

    public Table(Difficulty difficulty, Rabbit rabbit, Player player) {
        this.player = player;
        this.rabbit = rabbit;
        mapNumberPosition = new HashMap<>();
        allPosition = new ArrayList<>();
        switch (difficulty) {
            case EASY:
                table = initTable(DETAULT_ROW, DEFAULT_COLUMN);
                rabbit.setRabbitPosition(initRandomRabbitPosition(3));
                player.setLife(4);
                this.TOTAL_RABBIT = 3;
                break;
            case NORMAL:
                int rn = DETAULT_ROW * 2 + 1, cn = DEFAULT_COLUMN * 2 + 1;
                table = initTable(rn, cn);
                rabbit.setRabbitPosition(initRandomRabbitPosition(9));
                player.setLife(10);
                this.TOTAL_RABBIT = 9;
                break;
            case HARD:
                int rh = DETAULT_ROW * 4 + 1, ch = DEFAULT_COLUMN * 4 + 1;
                table = initTable(rh, ch);
                rabbit.setRabbitPosition(initRandomRabbitPosition(27));
                player.setLife(28);
                this.TOTAL_RABBIT = 27;
                break;
        }
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public void setRabbit(Rabbit rabbit) {
        this.rabbit = rabbit;
    }

    public void setTable(String[][] table) {
        this.table = table;
    }

    public Rabbit getRabbit() {
        return rabbit;
    }

    public int getTotalRabbit() { return TOTAL_RABBIT; }

    public HashMap<Integer, int[]> getMapPosition() {
        return mapNumberPosition;
    }

    public String[][] getTable() {
        return this.table;
    }

    public ArrayList<Integer> getAllPosition() { return allPosition; }

    private int[] initAllPosition(int len) {
        int[] result = new int[len];
        for(int i = 0; i < len; i++) {
            result[i] = i + 1;
        }
        return result;
    }

    private Set<Integer> initRandomRabbitPosition(int totalRabbit) {
        Set<Integer> result = new HashSet<>();
        Random rand = new Random();
        int i = totalRabbit;
        while(i > 0) {
            int pos = rand.nextInt(this.allPosition.size());
            if(!result.contains(pos)) {
                result.add(pos);
                i--;
            }
        }
        return result;
    }

    private String[][] initTable(int r, int c) {
        String[][] result = new String[r][c];
        int count = 1;
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                if (i % 2 == 0) {
                    result[i][j] = "-";
                } else if (j % 2 == 0) {
                    result[i][j] = "|";
                } else {
                    result[i][j] = String.valueOf(count);
                    mapNumberPosition.put(count, new int[] {i, j});
                    allPosition.add(count);
                    count++;
                }
            }
        }
        return result;
    }
}
