package ra;

public class Game {

    int playerLife = 0;

    int totalRabbit = 0;
    private Table classTable;

    public Game() {}

    public Game(Table table) {
        this.classTable = table;
        playerLife = this.classTable.getPlayer().life;
    }

    public void setTable(Table table) {
        this.classTable = table;
    }

    public Table getClassTable() {
        return classTable;
    }

    public void checkInput(int num) {
        if(num > classTable.getAllPosition().size()) {
            System.out.println("Input terlalu besar");
            return;
        }

//        if(num < classTable.getAllPosition().size()) return;

        int[] posMap = this.classTable.getMapPosition().get(num);
        String isChar = this.classTable.getTable()[posMap[0]][posMap[1]];

        if(isChar.equals("R") || isChar.equals("Z") ) {
            System.out.println("Tidak Boleh memilih kolom yang sama ya !!");
            return;
        }

        if(this.classTable.getRabbit().getRabbitPosition().contains(num)) {
            this.classTable.getTable()[posMap[0]][posMap[1]] = "R";
            this.totalRabbit++;
            printTable(this.classTable.getTable(), this.classTable.getTable().length, this.classTable.getTable()[0].length);
            System.out.println("WOW!!");
        } else {
            this.classTable.getTable()[posMap[0]][posMap[1]] = "Z";
            printTable(this.classTable.getTable(), this.classTable.getTable().length, this.classTable.getTable()[0].length);
            System.out.println("SALAH!!");
            this.playerLife--;
        }
    }

    public void printTable(String[][] table, int r, int c) {
        System.out.println();
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("Nyawa: " + this.playerLife+"\t\t\t\t Total kelinci: "+this.getClassTable().getTotalRabbit());
        System.out.println("Kelinci yang didapat: " + this.totalRabbit);
        System.out.println();
        for (int i = 0; i < r; i++) {

            for (int x = 0; x < 20; x++) {
                System.out.print(" ");
            }

            for (int j = 0; j < c; j++) {
                System.out.print(table[i][j]+ "\t");
            }

            System.out.println();
        }

        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
    }

    public void CHEAT() {
        System.out.println("Map: "+ this.getClassTable().getMapPosition());
        System.out.println("All position: "+ this.getClassTable().getAllPosition());
        System.out.println("Rabbits positiion: "+ this.classTable.getRabbit().getRabbitPosition());
    }
}


/*
    Riswan Agam
    04 - 08 - 2022
 */